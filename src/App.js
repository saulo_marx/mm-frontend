import React, { Component } from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import './App.css';
import styled from 'styled-components';
import Dictionary from './components/dictionary'

const Styles = styled.div`
`;

class App extends Component {
  render() {
    return (
      <Styles>
        <BrowserRouter>
          <Switch>
            <Route path={'/'} component={Dictionary}/>
          </Switch>
        </BrowserRouter>
      </Styles>
    );
  }
}

export default App;
